﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(IronPen.Startup))]
namespace IronPen
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();
        }
    }
}
