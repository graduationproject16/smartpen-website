//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IronPen.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Notification
    {
        public int Id { get; set; }
        public string SenderId { get; set; }
        public string ReciverId { get; set; }
        public string Description { get; set; }
        public int Status { get; set; }
        public System.DateTime Time { get; set; }
        public Nullable<int> GroupId { get; set; }
    }
}
