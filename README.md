# Smart Pen

Smartpen is a digital note-taking system that enables the user to take notes on regular paper
and see their notes transferred to their computer device where they can share using websites system in real time

# SmartPen Website
* SmartPen Website that allows the users to visualize their handwritten on their computers and smart devices and share with others in the real time.

* Users can have profiles and create album images with their notes.
* Users could create groups where the administrator of the group can create real-time sessions to share his writings with others
* Application allows group members to chat during the sessions.

* The Front-End is a template we bought and customized for our application.
* Project is created by Mahmoud Saad, Mohamed Mahmoud Kamel, and Ibrahim Mahdy.

# My participation in the project

* Mahmoud Saad was the coordinator to this project, he used to assign tasks for us to complete
* My participation in the project was minimal as I had to focus on the embedded system of the project, hence,I created Account, Profile Models, views, and controllers.
	
To check the project full documentation visit this [link](https://drive.google.com/file/d/1U_OQTZ40pwyhT3x178S9Y90uItywCBWm/view?usp=sharing)	

