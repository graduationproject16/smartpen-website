﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IronPen.Models;
using Microsoft.AspNet.Identity;
namespace IronPen.Controllers
{
    public class GroupsController : Controller
    {
        private IronPenEntities db = new IronPenEntities();

        // GET: Groups
        [Authorize]
        public ActionResult Index()
        {
            var userId = User.Identity.GetUserId();
            var userGroups = db.GroupVsUsers.Where(c => c.UserId == userId).Where(c=>c.Status==1);
            return View(userGroups);
        }

        // GET: Groups/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Group group = db.Groups.Find(id);
            if (group == null)
            {
                return HttpNotFound();
            }
            return View(group);
        }
        [Authorize]
        // GET: Groups/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Groups/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,GroupName")] Group group,string emails)
        {
            if (ModelState.IsValid)
            {
                group.CreateDate=DateTime.Now;
                group.Status = 1;//1 Active
                //Generate URL
                Guid g = Guid.NewGuid();
                string p = Convert.ToBase64String(g.ToByteArray());
                p = p.Replace("=", "");
                p = p.Replace("+", "");
                group.Url = p;
                db.Groups.Add(group);
                db.SaveChanges();
                //Admin Group Creation
                GroupVsUser groupVsUser = new GroupVsUser();
                groupVsUser.GroupId = group.Id;
                groupVsUser.UserId = User.Identity.GetUserId();
                groupVsUser.Role = 1; //Role = 1 Administrator
                groupVsUser.Status = 1;//Active User
                db.GroupVsUsers.Add(groupVsUser);
                db.SaveChanges();
                //Users Invited to the group
                var invitedUser = db.AspNetUsers.Where(c => c.Email == emails).First();
                GroupVsUser groupVsUser2 = new GroupVsUser();
                groupVsUser2.GroupId = group.Id;
                groupVsUser2.UserId = invitedUser.Id;
                groupVsUser2.Role = 2; //Role = 1 Normal User
                groupVsUser2.Status = 0;//invited User
                db.GroupVsUsers.Add(groupVsUser2);
                db.SaveChanges();
                Notification notification = new Notification();
                notification.SenderId = User.Identity.GetUserId();
                notification.ReciverId = invitedUser.Id;
                notification.Status = 0;
                notification.Time = DateTime.Now;
                notification.Description = "You are invited to join " + group.GroupName;
                notification.GroupId = group.Id;
                db.Notifications.Add(notification);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(group);
        }

        // GET: Groups/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Group group = db.Groups.Find(id);
            if (group == null)
            {
                return HttpNotFound();
            }
            return View(group);
        }

        // POST: Groups/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,GroupName")] Group group)
        {
            if (ModelState.IsValid)
            {
                db.Entry(group).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(group);
        }

        // GET: Groups/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Group group = db.Groups.Find(id);
            if (group == null)
            {
                return HttpNotFound();
            }
            return View(group);
        }

        // POST: Groups/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Group group = db.Groups.Find(id);
            db.Groups.Remove(group);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
