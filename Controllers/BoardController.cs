﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IronPen.Models;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;

namespace IronPen.Controllers
{
    public class BoardController : Controller
    {
        private IronPenEntities db = new IronPenEntities();

        [Authorize]
        // GET: Board
        public ActionResult Index(string p)
        {
            var users = db.AspNetUsers.Select(c => c.Email);
            ViewBag.users = users;
            var currentUserId = User.Identity.GetUserId();
            var userName = db.AspNetUsers.Where(c => c.Id == currentUserId).FirstOrDefault().UserName;
            ViewBag.userName = userName;
            var found = db.Groups.Where(c => c.Url == p).Count();
            if (found != 0)
            {
                var groupId = db.Groups.Where(c => c.Url == p).FirstOrDefault().Id;
                var canJoin =
                    db.GroupVsUsers.Where(c => c.GroupId == groupId)
                        .Where(c => c.UserId == currentUserId)
                        .Where(c => c.Status == 1).Where(c => c.Group.Status == 1).Count();
                if (canJoin == 0)
                {
                    TempData["errorMessage"] = "You are not allowed to join this Session";
                    return Redirect("~/Groups/");
                }


            }
            else
            {
                TempData["errorMessage"] = "This Session doesn`t exist";
                return Redirect("~/Groups/");
            }
            ViewData["IsNewGroup"] = false;
            ViewData["GroupName"] = p;
            ViewBag.GroupName = p;
            //allowed users in the Session
            var onlineUsers = db.GroupVsUsers.Where(c => c.Group.Url == p).Where(c => c.Status == 1);
            ViewBag.onlineUsers = onlineUsers;
            var OwnerId =
                db.GroupVsUsers.Where(c => c.Group.Url == p).Where(c => c.Role == 1)
                    .FirstOrDefault().UserId;
            var FeaturedUserId =
                db.GroupVsUsers.Where(c => c.Group.Url == p).Where(c => c.Feature == 1).Where(c=>c.UserId==currentUserId)
                    .Count();
            if (OwnerId == currentUserId)
            {
                ViewBag.Owner = 1;
            }
            else
            {
                ViewBag.Owner = 0;

            }
            if (FeaturedUserId == 1)
            {
                ViewBag.FeatureAccess = 1;
            }
            else
            {
                ViewBag.FeatureAccess = 0;

            }
            return View();
        }

        [Authorize]
        public ActionResult invite(List<String> email, string p)
        {
            var currentUser = User.Identity.GetUserId();
            var group = db.Groups.Where(c => c.Url == p).FirstOrDefault();

            foreach (var item in email)
            {
                var userInvited = db.AspNetUsers.Where(c => c.UserName == item).FirstOrDefault();
                var alreadyInvited =
                    db.GroupVsUsers.Where(c => c.UserId == userInvited.Id).Where(c => c.GroupId == group.Id).Count();
                if (alreadyInvited == 0)
                {
                    GroupVsUser groupVsUser = new GroupVsUser();
                    groupVsUser.UserId = userInvited.Id;
                    groupVsUser.GroupId = group.Id;
                    groupVsUser.Status = 0; //Invited user
                    groupVsUser.Role = 2;
                    db.GroupVsUsers.Add(groupVsUser);
                    db.SaveChanges();
                    Notification notification = new Notification();
                    notification.SenderId = currentUser;
                    notification.ReciverId = userInvited.Id;
                    notification.Time = DateTime.Now;
                    notification.Status = 0;
                    notification.Description = "You are invited to join " + group.GroupName;
                    notification.GroupId = group.Id;
                    db.Notifications.Add(notification);
                    db.SaveChanges();
                }
                else
                {
                    return Json(new {error = "This User is Already invited"});

                }

            }

            return Json(new {});
        }

        [Authorize]
        public ActionResult blockUser(string userId, string p)
        {
            var groupId = db.Groups.Where(c => c.Url == p).FirstOrDefault().Id;
            var blockedUser = db.AspNetUsers.Where(c => c.Id == userId).FirstOrDefault();
            var userData = db.GroupVsUsers.Where(c => c.GroupId == groupId).Where(c => c.UserId == blockedUser.Id).FirstOrDefault();
            var x = userData.Status;
            var result = "";
            if (x == 1)
            {
                userData.Status = 2;
                result = "UnBlock";
            }
            else
            {
                userData.Status = 1;
                result = "Block";
            }
            db.Entry(userData).State = EntityState.Modified;
            db.SaveChanges();
            return Json(new {success = result});
            

        }

        [Authorize]
        public ActionResult allowFeature(string userId, string p)
        {
            var groupId = db.Groups.Where(c => c.Url == p).FirstOrDefault().Id;
            var user = db.AspNetUsers.Where(c => c.Id == userId).FirstOrDefault();

            var userData=db.GroupVsUsers.Where(c => c.GroupId == groupId).Where(c => c.UserId == user.Id).FirstOrDefault();
            var x = userData.Feature;
            var result = "";
            if (x == 0)
            {
                userData.Feature = 1;
                result = "Disable";
            }
            else
            {
                userData.Feature = 0;
                result = "Allow";
            }
            db.Entry(userData).State = EntityState.Modified;
            db.SaveChanges();
            return Json(new {success = result});
        }

        public ActionResult saveImage(string base64,string p,string replay)
        {
            var groupId = db.Groups.Where(c => c.Url == p).FirstOrDefault().Id;
            base64 = base64.Replace("data:image/png;base64,", String.Empty);
            byte[] data = Convert.FromBase64String(base64);
            Image image;
            using (var stream = new MemoryStream(data, 0, data.Length))
            {
                image = Image.FromStream(stream);
                
            }
            Album imgAlbum = new Album();
            String unique = DateTime.Now.ToString("yyyyMMddHHmmssfff");
            var path = @"Content/Albums/" + p +"-"+ unique;
            var path2 = Server.MapPath("~/Content/Albums/" + p + "-" + unique);

            imgAlbum.ImageLink = path+".jpg";
            imgAlbum.FileLink = path+".txt";
            imgAlbum.GroupId = groupId;
            db.Albums.Add(imgAlbum);
            db.SaveChanges();
            image.Save(path2 + ".jpg", System.Drawing.Imaging.ImageFormat.Png);
            System.IO.File.WriteAllText(path2 + ".txt", replay);

            return Json(new {});
        }

    }
}
