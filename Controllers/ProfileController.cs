﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using IronPen.Models;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;

namespace IronPen.Controllers
{
    public class ProfileController : Controller
    {
        private IronPenEntities db = new IronPenEntities();
        // GET: Profile
        [Authorize]
        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        public ActionResult Albums()
        {
            var currentUser = User.Identity.GetUserId();
            List<int> groupIds=new List<int>();
            var joinedGroups = db.GroupVsUsers.Where(c => c.UserId == currentUser).Where(c=>c.Status==1);
            foreach (var item in joinedGroups)
            {
                groupIds.Add(item.GroupId);
            }
            ViewBag.GroupsIds = groupIds;
            var albums = db.Albums;
            return View(albums);
        }

        public ActionResult Page(int id)
        {
            var filePath = db.Albums.Where(c => c.Id == id).FirstOrDefault().FileLink;
            var text = System.IO.File.ReadAllText(Server.MapPath("~/"+filePath));
            ViewBag.test = new JavaScriptSerializer().Serialize(
    JsonConvert.SerializeObject(text, Formatting.None)
    );
            return View();
        }
        [Authorize]

        public ActionResult Notifications()
        {
            var currentUser = User.Identity.GetUserId();
            var notifications = db.Notifications.Where(c => c.ReciverId == currentUser).Where(c=>c.Status==0);
            return View(notifications);
        }

        [HttpPost]
        public ActionResult Notifications(string Accept,string Decline)
        { 
            var currentUser = User.Identity.GetUserId();
            var notifications = db.Notifications.Where(c => c.ReciverId == currentUser);
            if (Accept!=null)
            {
             int groupId = int.Parse(Accept);
                var invited =
                    db.Notifications.Where(c => c.GroupId == groupId)
                        .Where(c => c.ReciverId == currentUser).Where(c=>c.Status==0)
                        .OrderByDescending(c => c.Time)
                        .Count();
                if (invited != 0)
                {
                    var invitedInfo =
                        db.Notifications.Where(c => c.GroupId == groupId)
                            .Where(c => c.ReciverId == currentUser)
                            .OrderByDescending(c => c.Time).FirstOrDefault();
                    invitedInfo.Status = 1; //Accepted
                    db.Entry(invitedInfo).State = EntityState.Modified;
                    db.SaveChanges();
                    var groupVsUser = db.GroupVsUsers.Where(c => c.UserId == currentUser).Where(c => c.GroupId == groupId).FirstOrDefault();
                    groupVsUser.Status = 1;
                    db.Entry(groupVsUser).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    TempData["error"] = "Some Thing went wrong";
                }
            }
           

            return Redirect("~/Profile/Notifications");
        }
    }
}